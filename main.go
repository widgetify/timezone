package main

import (
	"fmt"
	"log"
	"encoding/json"
  	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello, ??")
}

func main() {
	http.HandleFunc("/", timezones)
	log.Fatal(http.ListenAndServe(":8888", nil))
}

func timezones(w http.ResponseWriter, r *http.Request) {

	type Timezone struct {
		Id      	int        `json:"id"`
		Zone      	string     `json:"zone"`
		Format    	string     `json:"format"`
	}
	timezonesList := []Timezone {
		Timezone {
			Id: 0,
			Zone: "US/Pacific",
			Format: "HH:mm:ss",
		},
		Timezone {
			Id: 1,
			Zone: "America/Danmarkshavn",
			Format: "HH:mm:ss",
		},
		Timezone {
			Id: 2,
			Zone: "Europe/Berlin",
			Format: "HH:mm:ss",
		},
		Timezone {
			Id: 3,
			Zone: "Africa/Algiers",
			Format: "HH:mm:ss",
		},
	}
  
	js, err := json.Marshal(timezonesList)
	if err != nil {
	  http.Error(w, err.Error(), http.StatusInternalServerError)
	  return
	}
  
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
    w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
    w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	w.Write(js)
  }